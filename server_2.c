#include <stdio.h>
#include <stdlib.h> // rand(), srand(), sleep(), EXIT_SUCCESS
#include <time.h> // time()
#include <unistd.h> // getpid(), getppid()
#include <stdbool.h>
#include <signal.h>

bool running = true;

void stop_handler(int sig) {
  printf("signal %d\n", sig);
  printf("stopping program...\n");
  running = false;
}

void exit_message() {
  printf("exit\n");
}

int main(void) {

  int exit = atexit(exit_message);

  struct sigaction action;

  action.sa_handler = stop_handler;
  action.sa_flags = 0;

  sigaction (SIGINT, &action, NULL);
  sigaction (SIGTERM, &action, NULL);

  srand(time(NULL));

  printf("server starting \n");

  while(running){
    printf("pid: %d, ppid: %d, random: %d\n", getpid(),getppid(),rand()%100);      sleep(1);
  }

  printf("server stopping\n");
  return EXIT_SUCCESS;
}
