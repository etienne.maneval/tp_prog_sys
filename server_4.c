#include <stdio.h>
#include <stdlib.h> // rand(), srand(), sleep(), EXIT_SUCCESS
#include <time.h> // time()
#include <unistd.h> // getpid(), getppid()
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <math.h>

bool running = true;

void stop_handler(int sig) {
  printf("signal %d\n", sig);
  printf("stopping program...\n");
  running = false;
}

void exit_message() {
  printf("exit\n");
}

int main(void) {
  int wstatus;
  int pipefd[2];
  
  pipe(pipefd);
  pid_t fork_id = fork();
  if (fork_id == -1) {
    exit(EXIT_FAILURE);
  }

  int exit = atexit(exit_message);

  struct sigaction action;

  action.sa_handler = stop_handler;
  action.sa_flags = 0;

  sigaction (SIGINT, &action, NULL);
  sigaction (SIGTERM, &action, NULL);

  srand(time(NULL));

  printf("server starting \n");

  while(running){
    if (fork_id == 0){
      char buffer[100];
      close(pipefd[1]);
      if (read(pipefd[0], buffer, 100)!=0){
        printf("pid: %d, ppid: %d, random: %s\n", getpid(),getppid(),buffer);
      } 
    }
    else {
      close(pipefd[0]);
      int random_number = rand()%100;
      int rand_num_size = snprintf( NULL, 0, "%d", random_number );
      char* rand_num_str = malloc( rand_num_size + 1 );
      snprintf( rand_num_str, rand_num_size + 1, "%d", random_number );
      write(pipefd[1], rand_num_str, strlen(rand_num_str)+1);
    }
    sleep(1);
  }
  printf("server stopping\n");
  return EXIT_SUCCESS;
}
