#include <stdio.h>
#include <stdlib.h> // rand(), srand(), sleep(), EXIT_SUCCESS
#include <time.h> // time()
#include <unistd.h> // getpid(), getppid()

int main(void) {

    srand(time(NULL));

    printf("server starting \n");

    while(1){
        printf("pid: %d, ppid: %d, random: %d\n", getpid(),getppid(),rand()%100);
        sleep(1);
    }

    printf("server stopping\n");
    return EXIT_SUCCESS;
}